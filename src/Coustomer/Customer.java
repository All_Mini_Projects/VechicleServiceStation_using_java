package Coustomer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

import Station.Bill;

public class Customer implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<String,Vehicle> vehList;
	private String name;
	private String mobileNumber;
	private String address;	
	public Customer() 
	{
		this.vehList=new HashMap<String, Vehicle>();
	}

	public String getAddress() {
		return address;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}
	public String getName() {
		return name;
	}
	public Vehicle getVehicle(String number)
	{
		return vehList.get(number);
	}
	
	public HashMap<String,Vehicle> getVehicleList()
	{
		return this.vehList;
	}

	public void newVehicle(Vehicle v)
	{
		this.vehList.put(v.getNumber(), v);
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setMobileNumber(String mobile) {
		this.mobileNumber = mobile;
	}
	public void setName(String name) {
		this.name = name;
	}

	
	
	@Override
	public String toString() {
		return "Customer [vehList=" + vehList + ", name=" + name + ", mobile=" + mobileNumber + ", address=" + address + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void acceptCustomer() throws IOException {
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Name : ");
		setName(sc.readLine());
		System.out.print("Enter Address : ");
		setAddress(sc.readLine());
		System.out.println("Enter mobile : ");
		setMobileNumber(sc.readLine());
	}

	public void acceptvehicel() throws IOException {
		Vehicle vehicle = new Vehicle();
		vehicle.acceptvehicel();
		vehList.put(vehicle.getNumber(), vehicle);
	}
	
	
}