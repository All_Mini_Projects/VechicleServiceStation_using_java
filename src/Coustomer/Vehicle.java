package Coustomer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;


public class Vehicle implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String companyName;
	private String model;
	private String number;
	
	public Vehicle() {
		this("","","");
	}

	public Vehicle(String companyName, String model, String number) {
		this.companyName = companyName;
		this.model = model;
		this.number = number;
	}
	
	//getters
	public String getCompanyName() {
		return companyName;
	}

	public String getModel() {
		return model;
	}

	public String getNumber() {
		return number;
	}

	//setters
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	@Override
	public boolean equals(Object obj) {
		Vehicle vehicle = (Vehicle) obj;
		return vehicle.number == this.number;
	}

	@Override
	public String toString() {
		return String.format("%-10s%-10s%-10s",this.companyName,this.model,this.number);
	}

	public void acceptvehicel() throws IOException {
			BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Company Name : ");
			setCompanyName(bf.readLine());
			System.out.println("Model : ");
			setModel(bf.readLine());
			System.out.println("Number : ");
			setNumber(bf.readLine());
	}
	
	
	
}
