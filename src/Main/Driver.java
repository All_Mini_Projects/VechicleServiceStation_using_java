
package Main;

import Station.*;
import Coustomer.Customer;
import Coustomer.Vehicle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Driver {
	static Scanner sc = new Scanner(System.in);
	static BufferedReader ss = new BufferedReader(new InputStreamReader(System.in));
	
	public static int mainManu() {
		System.out.println("0.Exit");
		System.out.println("1.Add New Customer");
		System.out.println("2.Req. For Service");
		System.out.println("3.Today's Business ");
		System.out.println("4.Given Day's Business ");
		System.out.print("Enter Choice : ");
		return sc.nextInt();	
	}
	
	public static int subManu() {
		System.out.println("0.Exit ");
		System.out.println("1.Oil");
		System.out.println("2.Maint");
		System.out.println("3.Done");
		System.out.print("Enter Choice");
		return sc.nextInt();
	}
	
	public static int partMenu()
	{
		System.out.println("0. Maintenance Done");
		System.out.println("1. Add New Part");
		System.out.print("Enter Your Choice: ");
		return sc.nextInt();
	}
	
	public static void addNewCustomer() throws IOException {
		ServiceStation.station.addCoustomer();
		ServiceStation.station.printCustomer();
	}
	
	private static void inputNewPart(Maintenance maintenance) {
			System.out.print("Enter Part Name: ");
			String name=sc.next();
			System.out.print("Enter Part Price: ");
			double price = sc.nextDouble();
			maintenance.newPart(name,price);	
	}

	private static void inputMaintenance(Maintenance maintenance) throws IOException {
		System.out.print("Enter Part Description: ");
		maintenance.setDescription(ss.readLine());
		System.out.print("Enter Part Price : ");
		maintenance.setPrice(sc.nextDouble());
	}

	private static void oilService(Oil oil) throws IOException {
		System.out.println("Enter Oil Desc : ");
		oil.setDescription(ss.readLine());
		System.out.println("Enter Cost Price : ");
		oil.setCost(sc.nextDouble());
	}

	public static void addNewRequest() throws IOException {
		
		Customer customer = null;
		if((customer  = ServiceStation.station.checkForCustomer() )!= null) {
				Vehicle vehicle = null;
				if((customer.getVehicleList()).isEmpty())
				{
					System.out.println("Vehicle Not Found");
					int num = 0;
					System.out.println("Enter 1  to Add Vehicle else ");
					num = sc.nextInt();
					if(num == 1)
						customer.acceptvehicel();
				}
				else {
					displayVehicleList(customer);
					System.out.println("Enter Vehicle Number : ");
					vehicle = customer.getVehicle(ss.readLine());
					
					if(vehicle != null)
						Driver.newServiceReq(customer,vehicle);
					else {
						System.out.println("Vehicle Not Found");
						int num = 0;
						System.out.println("Enter 1  to Add Vehicle else ");
						num = sc.nextInt();
						if(num == 1)
							customer.acceptvehicel();
					}
				}
		}
		else
			System.out.println("Customer Not Found !!!");

	}
	
	private static void newServiceReq(Customer customer, Vehicle vehicle) {
		int choice = 1;
		
		ServiceRequest service = ServiceStation.station.newServiceReq(customer.getName(),vehicle.getNumber());
		while(( choice = Driver.subManu()) != 0 ) {
			try {
				switch( choice ) {
				case 1:
					Oil oil = new Oil();
					oilService(oil);
					service.newService(oil);
					break;
				case 2:
					Maintenance maintenance = new Maintenance();
					inputMaintenance(maintenance);
					int choice2 = 0;
					while((choice2 = Driver.partMenu()) != 0) {
						switch( choice2 ) {
						case 1:
							inputNewPart(maintenance);
							break;
						default:
							System.out.println();
						}
					}
					service.newService(maintenance);
					break;
				
				case 3:			//Done
					Bill bill = ServiceStation.station.billGen(service);
					bill.printbill(System.out);
					System.out.print("Enter Paid Amount: ");
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private static void displayVehicleList(Customer customer) {
	
			for(String num :customer.getVehicleList().keySet())
				System.out.println(customer.getVehicleList().get(num));
	}

	public static double todayBusiness() {
		SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
		return ServiceStation.station.computeCash(simple.format(new Date()));
	}

	public static double BusinessAsPerDate() throws IOException {
		System.out.println("Enter Date Format(dd/MM/yyy) :");
		String date = ss.readLine();
		return ServiceStation.station.computeCash(date);
		
	}
	
}
