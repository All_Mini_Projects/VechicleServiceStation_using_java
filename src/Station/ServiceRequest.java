package Station;

import java.util.List;

import java.io.Serializable;

import java.util.LinkedList;

public class ServiceRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customerName ;
	private List<Service> serviceList;
	private String vehicleNumber ;
	
	public ServiceRequest() {
		this.serviceList= ( new LinkedList<>());
	}
	public ServiceRequest(String customerName, String vehicleNumber) {
		serviceList= (new LinkedList<Service>());
		this.customerName = customerName;
		this.vehicleNumber = vehicleNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public List<Service> getServiceList() {
		return serviceList;
	}
	public void setServiceList(Service desc) {
		serviceList.add(desc);
	}
	
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	
	public void printSericeList() {
		for (Service service : serviceList) {
			System.out.println(service.toString());
		}
	}
	
	
	
	@Override
	public String toString() {
		return "ServiceRequest [customerName=" + customerName + ", serviceList=" + serviceList + ", vehicleNumber="
				+ vehicleNumber + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vehicleNumber == null) ? 0 : vehicleNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceRequest other = (ServiceRequest) obj;
		if (vehicleNumber == null) {
			if (other.vehicleNumber != null)
				return false;
		} else if (!vehicleNumber.equals(other.vehicleNumber))
			return false;
		return true;
	}
	public void newService(Service s)
	{
		this.serviceList.add(s);
	}
	
	
}
