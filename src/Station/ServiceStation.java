package Station;

import java.util.List;
import java.util.Set;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import Coustomer.*;

public class ServiceStation implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final ServiceStation station=new ServiceStation();
	private String nameOfServiceStation = "Power Plus";
	private Set<Customer> customerList ;
	private List<Bill> billList;
	static BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
	
	public ServiceStation() {
		customerList = ( new HashSet<>() );
		 billList = ( new LinkedList<>());
		 try {
			readBillList();
			readCustomerList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
	public void saveCustomerList( ) throws Exception{
		try( ObjectOutputStream outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("src/Files/Customer.db"))))){
			outputStream.writeObject(this.customerList);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void readCustomerList( ) throws Exception{
		try( ObjectInputStream inputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File("src/Files/Customer.db"))))){
			this.customerList =  (Set<Customer>) inputStream.readObject();
		}
	}
	
	public void saveBillList() throws Exception {
		try(ObjectOutputStream outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("src/Files/Bills.db"))))){
			outputStream.writeObject(this.billList);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void readBillList() throws Exception {
		try( ObjectInputStream inputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File("src/Files/Bills.db"))))){
			this.billList = (List<Bill>) inputStream.readObject();
		}
	}
	
	public ServiceStation(String nameOfServiceStation) {
		customerList = ( new HashSet<>() );
		this.nameOfServiceStation = nameOfServiceStation;
		 billList = ( new LinkedList<>());
	}

	public String getNameOfServiceStation() {
		return nameOfServiceStation;
	}

	public void setNameOfServiceStation(String nameOfServiceStation) {
		this.nameOfServiceStation = nameOfServiceStation;
	}
	
	public void addCoustomer() throws IOException {
		Customer customer = new Customer();
		customer.acceptCustomer();
		customer.acceptvehicel();
		customerList.add(customer);
	}

	public Customer checkForCustomer() throws IOException {
		System.out.println("Enter Name : ");
		String name = sc.readLine();
		for(Customer cus : customerList) {
			//System.out.println(cus);
			if(cus.getName().equals(name))
				return cus;
		}
		return null;
	}
	
	public void printCustomer() {
		Customer cus = new Customer();
			System.out.println(this.customerList.contains(cus));
		
	}

	public ServiceRequest newServiceReq(String name, String number) {
			return new ServiceRequest(name,number);
		}

	public Bill billGen(ServiceRequest service) {
		Bill bill=new Bill();
		SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String date2 = simple.format(date);
		bill.setDate(date2);
		bill.setSer(service);
		newBill(bill);
		return bill;
	}
	
	private void newBill(Bill bill) {
		this.billList.add(bill);
		
	}
	
	public double computeCash(String date)
	{
		double total = 0;
		for(Bill bill : this.billList) {
			String billDate = bill.getDate();
			if(date.equals(billDate)) {
				total = total + bill.getPaidAmount();
			}
			}
		return total;
	}
	
}
	
