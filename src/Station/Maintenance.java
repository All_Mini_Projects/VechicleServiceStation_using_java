package Station;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Maintenance extends Service implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double price;
	private List<Part> partList;
	
	
	public Maintenance(double price) {
		this.price = price;
		partList = ( new LinkedList<>() );
	}


	public Maintenance() {
		partList = ( new LinkedList<>() );
	}
	
	public void setPrice(double price) {
		this.price = price;
	}

	
	public double getLaborCharges() {
		return price;
	}

	public void display() {
		System.out.println(this.getDescription());
		System.out.println("Labort Charges : ");
		
	}

//	public void input() throws Exception {
//		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
//		System.out.println("Enter Part Desc : ");
//		setDescription(bf.readLine());
//		System.out.println("Enter Price : ");
//		double price = Double.parseDouble(bf.readLine());
//	}

	@Override
	public double price() {
		double price = 0;
		for(Part s : partList) {
			price = price + s.getRate();
		}
		
		return price;
	}
	
	public List<Part> getPartList() {
		return partList;
	}

	public void newPart(String name, double price2) {
		Part part = new Part(name,price2);
		partList.add(part);
		
	}
	
}
