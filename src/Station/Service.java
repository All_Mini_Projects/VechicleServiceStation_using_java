package Station;

import java.io.Serializable;

abstract public class Service implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String description;	
	
	public Service() {
		
	}
	
	public Service(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return String.format("%-20s", this.description);
	}


	public abstract double price();
	
	
}
