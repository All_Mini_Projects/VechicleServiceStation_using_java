package Station;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;

import java.io.Serializable;


public class Part implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String description;	
	private double rate;
	public Part() {
	
	}
	
	public Part(String description, double rate) {
		this.description = description;
		this.rate = rate;
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}
	
	public void acceptPart() {
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			System.out.print("Enter Part Name : ");
			setDescription(bf.readLine());
			System.out.print("Enter Part rate : ");
			String value = bf.readLine();
			double rate = Double.parseDouble(value);
			setRate(rate);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Override
	public String toString() {
		return String.format("%-20s%-15s", this.description,this.rate);
	}

	@Override
	public boolean equals(Object obj) {
		Part part = (Part) obj;
		return (this.getDescription() == part.getDescription());
	}
	
	
	
}
