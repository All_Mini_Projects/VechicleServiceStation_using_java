package Station;


import java.util.Scanner;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Serializable;

public class Bill implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double amount;
	private double paidAmount;
	private String date;
	private ServiceRequest ser;
	
	
	public Bill() {
	
	}
	
	public Bill(double amount, double paidAmount,String date,ServiceRequest ser) {
		this.amount = amount;
		this.paidAmount = paidAmount;
		this.date = date;
		this.ser = ser;
	}
	
	public double computeAmount()
	{
		double to = 0;
		for(Service s :this.ser.getServiceList())
			to = to + s.price();
		this.amount = to;
		return to;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}


	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	
	public double computeTax() {
		double tax =getAmount()*0.12;
		return tax;
	}
	
	public double labourCost() {
		return 1200;
	}

	public ServiceRequest getSer() {
		return ser;
	}

	public void setSer(ServiceRequest ser) {
		this.ser = ser;
	}

	public void printbill(OutputStream out) {
		Scanner sc = new Scanner(System.in);
		
		PrintStream pr = new PrintStream(out);
		pr.println("*************************************************");
		pr.println("Date: "+this.date);
		pr.println("Customer Name: "+this.getSer().getCustomerName());
		pr.println("Vehicle No.: "+this.getSer().getVehicleNumber());
		pr.println("*************************************************");
		for(Service service :this.getSer().getServiceList())
		{
			if( service instanceof Oil ) {
				pr.println("Oil Name: "+service.getDescription());
				Oil oil =(Oil) service;
				pr.println("Price: "+oil.getCost());
				pr.println("Total Amount: "+computeAmount());
				
			}
			else
			{
				
				Maintenance maintenance=(Maintenance) service;
				System.out.print("Enter Labor Charges : ");
				pr.println("Labor Charges: "+sc.nextDouble());
				pr.println("Maintenance Description: ");
				for(Part part : maintenance.getPartList())
					pr.println(part.toString());
				pr.println("Sub Total: "+maintenance.price());
				pr.println("Amount: "+computeAmount());
				pr.println("Tax: "+ computeTax());
				pr.println("Total Amount: "+(computeAmount() + computeTax()));
				
			}
			pr.println("+ =============================================== +");
			System.out.print("Enter Paid Amount: ");
			double Paid = sc.nextDouble();
			setPaidAmount(Paid);
			System.out.println("Panding : "+(this.amount-this.paidAmount));
			
			
		}
	}
	
}
