package Station;

import java.io.Serializable;


public class Oil extends Service implements Serializable{
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private double cost;

		public Oil() {
			
		}
		public Oil(double cost) {
			this.cost = cost;
		}

		public void oilService() {
			setDescription("Oil Change ");
		}
		public double getCost() {
			return cost;
		}

		public double price()
		{
			return cost;
		}
		public void setCost(double cost) {
			this.cost = cost;
		}
		
		@Override
		public String toString() {
			return String.format("%-10s%-10d",this.getDescription(),this.cost);
		}
	
}
